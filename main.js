document.getElementById("defaultOpen").click();

function openTab(evt, tab) {
   var i, tabcontent, tablinks;

   tabcontent = document.getElementsByClassName("tabcontent");
   for (i = 0; i < tabcontent.length; i++) {
     tabcontent[i].style.display = "none";
   }

   tablinks = document.getElementsByClassName("tablinks");
   for (i = 0; i < tablinks.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" active", "");
   }

   document.getElementById(tab).style.display = "block";
   evt.currentTarget.className += " active";

   document.getElementById("tabtitle").innerHTML = tab;
}

function aud(){
  var bgm = document.getElementById("bgm");
  if (bgm.paused) {
    bgm.play();
    document.getElementById("sound").innerHTML = "♬";
  } else {
    bgm.pause();
    document.getElementById("sound").innerHTML = "<s>♬</s>";
  }
}

var waste = 0;
var material = 0;
var globalmat = 0;
var collectnb = 1;
var wastechance = 0.5;
var materialchance = 0;

function uppower(){
  var recruitCost = Math.floor(15 * Math.pow(1.3,(collectnb-1)));
  if (material>=recruitCost){
    collectnb = collectnb + 1;
    material = material - recruitCost;
    document.getElementById('clickpower').innerHTML = collectnb;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(15 * Math.pow(1.3,(collectnb-1)));
  document.getElementById('powerCost').innerHTML = nextCost;
}

var upthingnb = 0;
function upthing(){
  var recruitCost = Math.floor(30 * Math.pow(1.8,upthingnb));
  if (material>=recruitCost && wastechance < 0.97){
    upthingnb = upthingnb + 1;
    wastechance = wastechance + 0.05;
    material = material - recruitCost;
    updateinfo();
  }
  var nextCost = Math.floor(30 * Math.pow(1.8,upthingnb));
  document.getElementById('wasteCost').innerHTML = nextCost;
}

var upmatnb = 0;
function upmaterial(){
  var recruitCost = Math.floor(50 * Math.pow(1.8,upmatnb));
  if (material>=recruitCost && materialchance < 0.46){
    upmatnb = upmatnb + 1;
    materialchance = materialchance + 0.05;
    material = material - recruitCost;
    updateinfo();
  }
  var nextCost = Math.floor(50 * Math.pow(1.8,upmatnb));
  document.getElementById('matCost').innerHTML = nextCost;
}

function collect(){
  var chance = Math.random();
  if(chance < materialchance){
    material = material + collectnb;
    globalmat = globalmat + collectnb;
    document.getElementById("totalmat").innerHTML = numberformat.formatShort(globalmat, {sigfigs: 6});
    document.getElementById("material").innerHTML = numberformat.formatShort(material, {sigfigs: 6});
    document.getElementById("clickspace").style.background="#4c594a";
  }else if (chance < wastechance) {
    waste = waste + collectnb;
    document.getElementById("waste").innerHTML = numberformat.formatShort(waste, {sigfigs: 6});
    document.getElementById("clickspace").style.background="rgb(60,60,60)";

  }else{
    document.getElementById("clickspace").style.background="rgb(253, 253, 253)";
  }
}

function updateinfo(){
  document.getElementById("totalmat").innerHTML = numberformat.formatShort(globalmat, {sigfigs: 6});
  document.getElementById("material").innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  document.getElementById("waste").innerHTML = numberformat.formatShort(waste, {sigfigs: 6});
  document.getElementById("wastechance").innerHTML = Math.round ((wastechance-materialchance)*100);
  document.getElementById("materialchance").innerHTML = Math.round (materialchance*100);
  document.getElementById("nothingchance").innerHTML = Math.round ((1-wastechance)*100);
  document.getElementById("clickpower").innerHTML = collectnb;
  wastesec = (0.1*volunteers+cleaners+10*orgs+100*stations+1000*machines)*multiplier;
  document.getElementById("wastesec").innerHTML = numberformat.formatShort(wastesec, {sigfigs: 6});
  document.getElementById("something").innerHTML =  Math.round (wastechance*100);
}

function sellbuy(){
  var change = waste;
  waste = 0;
  material = material + Math.floor(change/50);
  globalmat = globalmat + Math.floor(change/50);
  updateinfo();
}

recycle12 = 0;
recycle22 = 0;
recycle32 = 0;
recycle42 = 0;
recycle52 = 0;
recycle62 = 0;
recycle72 = 0;
recycle82 = 0;
recycle92 = 0;
recyclea2 = 0;
recycleb2 = 0;
recyclec2 = 0;

cdmultiplier = 1;
function recycle(id,cost,cd,output){
  if (waste>=cost && document.getElementById(id).style.pointerEvents != 'none'){
    cd = cd * cdmultiplier;
    document.getElementById(id).style.pointerEvents = 'none';
    waste = waste - cost;
    document.getElementById("waste").innerHTML = numberformat.formatShort(waste, {sigfigs: 6});
    var startTime = Date.now();
    var move = setInterval(function(){
    document.getElementById(id+"1").style.width = (Date.now()-startTime)/cd*100 + "%";
    },50);
    setTimeout(function(){
      material = material + output;
      globalmat = globalmat + output;
      document.getElementById("material").innerHTML = numberformat.formatShort(material, {sigfigs: 6});
      document.getElementById("totalmat").innerHTML = numberformat.formatShort(globalmat, {sigfigs: 6});
      clearInterval(move);
      document.getElementById(id+"1").style.width = 0;
      document.getElementById(id).style.pointerEvents = 'auto';
      if(window[id+'2'] == 1){
        recycle(id,cost,cd/cdmultiplier,output);
      }
    }, cd);
  }
}

var wastesec = 0;

window.setInterval(function(){
  updateinfo();
  if (recycle12 == 1){
    recycle('recycle1',5,5000,1);
  }
  if (recycle22 == 1){
    recycle('recycle2',20,12000,4)
  }
  if (recycle32 == 1){
    recycle('recycle3',80,60000,20);
  }
  if (recycle42 == 1){
    recycle('recycle4',100,15000,22);
  }
  if (recycle52 == 1){
    recycle('recycle5',500,40000,200);
  }
  if (recycle62 == 1){
    recycle('recycle6',1000,60000,360);
  }
  if (recycle72 == 1){
    recycle('recycle7',4000,90000,2000);
  }
  if (recycle82 == 1){
    recycle('recycle8',12000,180000,5000);
  }
  if (recycle92 == 1){
    recycle('recycle9',30000,60000,16000);
  }
  if (recyclea2 == 1){
    recycle('recyclea',320000,60000,200000);
  }
  if (recycleb2 == 1){
    recycle('recycleb',10000000,180000,8000000);
  }
  if (recyclec2 == 1){
    recycle('recyclec',60000000,30000,54000000);
  }
}, 2000);

function autorec(id){
  var sillyString = id.slice(0, -1) + "auto";
  if (window[id] == 1){
    window[id]=0;
    document.getElementById(sillyString).style.opacity= "0.5";
  }else{
    window[id]=1;
    document.getElementById(sillyString).style.opacity= "1";
  }
}

var volunteers = 0;
function recruit(){
  var recruitCost = Math.floor(2 * Math.pow(1.10,volunteers));
  if (material>=recruitCost){
    volunteers = volunteers + 1;
    material = material - recruitCost;
    document.getElementById('volunteers').innerHTML = volunteers;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(2 * Math.pow(1.10,volunteers));
  document.getElementById('vCost').innerHTML = nextCost;
}

var cleaners = 0;
function hirecleaner(){
  var recruitCost = Math.floor(10 * Math.pow(1.19,cleaners));
  if (material>=recruitCost){
    cleaners = cleaners + 1;
    material = material - recruitCost;
    document.getElementById('cleaners').innerHTML = cleaners;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(10 * Math.pow(1.19,cleaners));
  document.getElementById('cCost').innerHTML = nextCost;
}

var orgs = 0;
function recruitorg(){
  var recruitCost = Math.floor(100 * Math.pow(1.17,orgs));
  if (material>=recruitCost){
    orgs = orgs + 1;
    material = material - recruitCost;
    document.getElementById('orgs').innerHTML = orgs;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(100 * Math.pow(1.17,orgs));
  document.getElementById('oCost').innerHTML = nextCost;
}

var stations = 0;
function buystation(){
  var recruitCost = Math.floor(1600 * Math.pow(1.15,stations));
  if (material>=recruitCost){
    stations = stations + 1;
    material = material - recruitCost;
    document.getElementById('stations').innerHTML = stations;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(1600 * Math.pow(1.15,stations));
  document.getElementById('sCost').innerHTML = nextCost;
}

var machines = 0;
function buymachine(){
  var recruitCost = Math.floor(20000 * Math.pow(1.2,machines));
  if (material>=recruitCost){
    machines = machines + 1;
    material = material - recruitCost;
    document.getElementById('machines').innerHTML = machines;
    document.getElementById('material').innerHTML = numberformat.formatShort(material, {sigfigs: 6});
  }
  var nextCost = Math.floor(20000 * Math.pow(1.2,machines));
  document.getElementById('mCost').innerHTML = nextCost;
}

var multiplier = 1;
var tick = 1000;
window.setInterval(function(){
  waste = waste + (0.1*volunteers+cleaners+10*orgs+100*stations+1000*machines)*multiplier;
  document.getElementById("waste").innerHTML = numberformat.formatShort(waste, {sigfigs: 6});
}, tick);

function unlock(id,cost){
  if (material>=cost && document.getElementById(id).style.display != "inline-block"){
    material = material - cost;
    updateinfo();
    document.getElementById(id).style.display="inline-block";
    document.getElementById(id+"t").style.display="none";
  }
}

function save(){
  var saverec = {
    waste: waste,
    material: material,
    collectnb: collectnb,
    wastechance: wastechance,
    materialchance: materialchance,
    upthingnb: upthingnb,
    upmatnb: upmatnb,
    volunteers: volunteers,
    cleaners: cleaners,
    orgs: orgs,
    stations: stations,
    machines: machines,
    recycle1: document.getElementById('recycle1').style.display,
    recycle2: document.getElementById('recycle2').style.display,
    recycle3: document.getElementById('recycle3').style.display,
    recycle4: document.getElementById('recycle4').style.display,
    recycle5: document.getElementById('recycle5').style.display,
    recycle6: document.getElementById('recycle6').style.display,
    recycle7: document.getElementById('recycle7').style.display,
    recycle8: document.getElementById('recycle8').style.display,
    recycle9: document.getElementById('recycle9').style.display,
    recyclea: document.getElementById('recyclea').style.display,
    recycleb: document.getElementById('recycleb').style.display,
    recyclec: document.getElementById('recyclec').style.display,
    recycle1auto: document.getElementById('recycle1auto').style.display,
    recycle2auto: document.getElementById('recycle2auto').style.display,
    recycle3auto: document.getElementById('recycle3auto').style.display,
    recycle4auto: document.getElementById('recycle4auto').style.display,
    recycle5auto: document.getElementById('recycle5auto').style.display,
    recycle6auto: document.getElementById('recycle6auto').style.display,
    recycle7auto: document.getElementById('recycle7auto').style.display,
    recycle8auto: document.getElementById('recycle8auto').style.display,
    recycle9auto: document.getElementById('recycle9auto').style.display,
    recycleaauto: document.getElementById('recycleaauto').style.display,
    recyclebauto: document.getElementById('recyclebauto').style.display,
    recyclecauto: document.getElementById('recyclecauto').style.display
  }
  localStorage.setItem("saverec",JSON.stringify(saverec));
  var saverecp = {
    cdmultiplier: cdmultiplier,
    multiplier: multiplier,
    tick: tick,
    globalmat: globalmat,
  }
  localStorage.setItem("saverecp",JSON.stringify(saverecp));
}

function loadex(){
  var savegamep = JSON.parse(localStorage.getItem("saverecp"));
  if (typeof savegamep.globalmat !== "undefined") globalmat = savegamep.globalmat;
  if (typeof savegamep.cdmultiplier !== "undefined") cdmultiplier = savegamep.cdmultiplier;
  if (typeof savegamep.multiplier !== "undefined") multiplier = savegamep.multiplier;
  console.log(savegamep.globalmat);
  console.log(globalmat);
  document.getElementById('prestigecur').innerHTML = multiplier;
}
function load(){
  var savegamep = JSON.parse(localStorage.getItem("saverecp"));
  var savegame = JSON.parse(localStorage.getItem("saverec"));
  if (typeof savegame.waste !== "undefined") waste = savegame.waste;
  if (typeof savegame.material !== "undefined") material = savegame.material;
  if (typeof savegame.collectnb !== "undefined") collectnb = savegame.collectnb;
  if (typeof savegame.wastechance !== "undefined") wastechance = savegame.wastechance;
  if (typeof savegame.materialchance !== "undefined") materialchance = savegame.materialchance;
  if (typeof savegame.upthingnb !== "undefined") upthingnb = savegame.upthingnb;
  if (typeof savegamep.globalmat !== "undefined") globalmat = savegamep.globalmat;
  if (typeof savegame.upmatnb !== "undefined") upmatnb = savegame.upmatnb;
  updateinfo();
  console.log(savegamep.multiplier);
  console.log(savegame.waste);
  var powerCost = Math.floor(15 * Math.pow(1.3,collectnb));
  var wasteCost = Math.floor(30 * Math.pow(1.8,upthingnb));
  var matCost = Math.floor(50 * Math.pow(1.8,upmatnb));
  document.getElementById('powerCost').innerHTML = powerCost;
  document.getElementById('wasteCost').innerHTML = wasteCost;
  document.getElementById('matCost').innerHTML = matCost;



  if (typeof savegame.volunteers !== "undefined") volunteers = savegame.volunteers;
  document.getElementById('volunteers').innerHTML = volunteers;
  if (typeof savegame.cleaners !== "undefined") cleaners = savegame.cleaners;
  document.getElementById('cleaners').innerHTML = cleaners;
  if (typeof savegame.orgs !== "undefined") orgs = savegame.orgs;
  document.getElementById('orgs').innerHTML = orgs;
  if (typeof savegame.stations !== "undefined") stations = savegame.stations;
  document.getElementById('stations').innerHTML = stations;
  if (typeof savegame.machines !== "undefined") machines = savegame.machines;
  document.getElementById('machines').innerHTML = machines;

  if (typeof savegamep.cdmultiplier !== "undefined") cdmultiplier = savegamep.cdmultiplier;
  if (typeof savegamep.multiplier !== "undefined") multiplier = savegamep.multiplier;
  if (typeof savegamep.tick !== "undefined") tick = savegamep.tick;
  document.getElementById('recycle1').style.display= savegame.recycle1;
  document.getElementById('recycle2').style.display= savegame.recycle2;
  document.getElementById('recycle3').style.display= savegame.recycle3;
  document.getElementById('recycle4').style.display= savegame.recycle4;
  document.getElementById('recycle5').style.display= savegame.recycle5;
  document.getElementById('recycle6').style.display= savegame.recycle6;
  document.getElementById('recycle7').style.display= savegame.recycle7;
  document.getElementById('recycle8').style.display= savegame.recycle8;
  document.getElementById('recycle9').style.display= savegame.recycle9;
  document.getElementById('recyclea').style.display= savegame.recyclea;
  document.getElementById('recycleb').style.display= savegame.recycleb;
  document.getElementById('recyclec').style.display= savegame.recyclec;
  document.getElementById('recycle1auto').style.display= savegame.recycle1auto;
  document.getElementById('recycle2auto').style.display= savegame.recycle2auto;
  document.getElementById('recycle3auto').style.display= savegame.recycle3auto;
  document.getElementById('recycle4auto').style.display= savegame.recycle4auto;
  document.getElementById('recycle5auto').style.display= savegame.recycle5auto;
  document.getElementById('recycle6auto').style.display= savegame.recycle6auto;
  document.getElementById('recycle7auto').style.display= savegame.recycle7auto;
  document.getElementById('recycle8auto').style.display= savegame.recycle8auto;
  document.getElementById('recycle9auto').style.display= savegame.recycle9auto;
  document.getElementById('recycleaauto').style.display= savegame.recycleaauto;
  document.getElementById('recyclebauto').style.display= savegame.recyclebauto;
  document.getElementById('recyclecauto').style.display= savegame.recyclecauto;
  if (document.getElementById('recycle1').style.display == "inline-block"){
    document.getElementById('recycle1t').style.display="none";
  }
  if (document.getElementById('recycle2').style.display == "inline-block"){
    document.getElementById('recycle2t').style.display="none";
  }
  if (document.getElementById('recycle3').style.display == "inline-block"){
    document.getElementById('recycle3t').style.display="none";
  }
  if (document.getElementById('recycle4').style.display == "inline-block"){
    document.getElementById('recycle4t').style.display="none";
  }
  if (document.getElementById('recycle5').style.display == "inline-block"){
    document.getElementById('recycle5t').style.display="none";
  }
  if (document.getElementById('recycle6').style.display == "inline-block"){
    document.getElementById('recycle6t').style.display="none";
  }
  if (document.getElementById('recycle7').style.display == "inline-block"){
    document.getElementById('recycle7t').style.display="none";
  }
  if (document.getElementById('recycle8').style.display == "inline-block"){
    document.getElementById('recycle8t').style.display="none";
  }
  if (document.getElementById('recycle9').style.display == "inline-block"){
    document.getElementById('recycle9t').style.display="none";
  }
  if (document.getElementById('recyclea').style.display == "inline-block"){
    document.getElementById('recycleat').style.display="none";
  }
  if (document.getElementById('recycleb').style.display == "inline-block"){
    document.getElementById('recyclebt').style.display="none";
  }
  if (document.getElementById('recyclec').style.display == "inline-block"){
    document.getElementById('recyclect').style.display="none";
  }
  if (document.getElementById('recycle1auto').style.display == "inline-block"){
    document.getElementById('recycle1autot').style.display="none";
  }
  if (document.getElementById('recycle2auto').style.display == "inline-block"){
    document.getElementById('recycle2autot').style.display="none";
  }
  if (document.getElementById('recycle3auto').style.display == "inline-block"){
    document.getElementById('recycle3autot').style.display="none";
  }
  if (document.getElementById('recycle4auto').style.display == "inline-block"){
    document.getElementById('recycle4autot').style.display="none";
  }
  if (document.getElementById('recycle5auto').style.display == "inline-block"){
    document.getElementById('recycle5autot').style.display="none";
  }
  if (document.getElementById('recycle6auto').style.display == "inline-block"){
    document.getElementById('recycle6autot').style.display="none";
  }
  if (document.getElementById('recycle7auto').style.display == "inline-block"){
    document.getElementById('recycle7autot').style.display="none";
  }
  if (document.getElementById('recycle8auto').style.display == "inline-block"){
    document.getElementById('recycle8autot').style.display="none";
  }
  if (document.getElementById('recycle9auto').style.display == "inline-block"){
    document.getElementById('recycle9autot').style.display="none";
  }
  if (document.getElementById('recycleaauto').style.display == "inline-block"){
    document.getElementById('recycleaautot').style.display="none";
  }
  if (document.getElementById('recyclebauto').style.display == "inline-block"){
    document.getElementById('recyclebautot').style.display="none";
  }
  if (document.getElementById('recyclecauto').style.display == "inline-block"){
    document.getElementById('recyclecautot').style.display="none";
  }
  var vCost = Math.floor(2 * Math.pow(1.10,volunteers));
  document.getElementById('vCost').innerHTML = vCost;
  var cCost = Math.floor(10 * Math.pow(1.19,cleaners));
  document.getElementById('cCost').innerHTML = cCost;
  var oCost = Math.floor(100 * Math.pow(1.17,orgs));
  document.getElementById('oCost').innerHTML = oCost;
  var sCost = Math.floor(1600 * Math.pow(1.15,stations));
  document.getElementById('sCost').innerHTML = sCost;
  var mCost = Math.floor(20000 * Math.pow(1.2,machines));
  document.getElementById('mCost').innerHTML = mCost;
  document.getElementById('prestigecur').innerHTML = multiplier;
}

function deletesave(){
  localStorage.removeItem("saverec");
}

window.setInterval(function(){
  save();
  submitthings();
}, 30000);

function prestigecal(){
  document.getElementById('prestigeop').innerHTML =  1+0.1*Math.floor( Math.sqrt(globalmat/3e6) );
  document.getElementById('prestigecur').innerHTML = multiplier;
}

function prestige(){
  multiplier = 1+0.1*Math.floor( Math.sqrt(globalmat/3e6));
  cdmultiplier = 1/multiplier;
  save();
  deletesave();
  window.location.reload(true);
  loadex();
}

kongregateAPI.loadAPI(function(){
  window.kongregate = kongregateAPI.getAPI();
});

function submitthings(){
  kongregate.stats.submit('globalmat', globalmat);
  kongregate.stats.submit('material', material);
  kongregate.stats.submit('multiplier', multiplier);
}