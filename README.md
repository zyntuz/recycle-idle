# Recycle Idle

Recycle Idle is an incremental game about recycling.

Originally published on [Kongregate](https://www.kongregate.com/games/slimeow/recycle-idle).

You can now play it [here](https://zyntuz.gitlab.io/recycle-idle).

## Licensing

All code are licensed under the terms of the MIT license.

`.png` files are either in public domain or licensed under the Pixabay License.

`bgm.mp3` ("Warm Lights" by Nicolai Heidlas) is licensed under CC BY 3.0.
